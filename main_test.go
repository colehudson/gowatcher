package main

import "testing"

func Testmain(t *testing.T) {
	got := main()
	want := "modified file"

	if got != want {
		t.Errorf("got %q want %q", got, want)
	}
}
