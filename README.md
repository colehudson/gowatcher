# GoWatcher

A project to work through dependency management and testing in GoLang. It uses the fsnotify example (to watch specified directories for activity) as its main driver.


`go mod init MODULE_PATH` creates the go.mod file.

`go mody tidy`


Sample test runs with `go test`.